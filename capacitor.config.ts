import { CapacitorConfig } from '@capacitor/cli';

let config: CapacitorConfig;

const baseConfig: CapacitorConfig = {
  webDir: 'www/browser',
  server: {
    androidScheme: 'https'
  }
};

switch (process.env['NODE_ENV']) {
  case 'production':
    config = {
      ...baseConfig,
      appId: 'fr.stapa.staparty',
      appName: 'StaPa_rty!',
      android: {
        flavor: 'production'
      }
    };
    break;
  case 'production-ios':
    config = {
      ...baseConfig,
      appId: 'fr.stapa.staparty',
      appName: 'StaPa_rty!',
      server: {
        hostname: 'api.stapa.fr'
      }
    };
    break
  default:
    config = {
      ...baseConfig,
      appId: 'fr.stapa.staparty.staging',
      appName: 'StaPa_rty! DEMO',
      android: {
        flavor: 'staging'
      }
    };
    break;
}

export default config;
