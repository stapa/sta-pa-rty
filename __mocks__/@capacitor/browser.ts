export interface OpenOptions {
    url: string,
    windowName?: string
}

export class PluginListenerHandle {
    constructor(
        public listenerFunc: () => void,
        private testRemoveListenerFn: (handle: PluginListenerHandle) => void
    ) { }

    remove() {
        this.testRemoveListenerFn(this);
    }

}

export class BrowserPlugin {
    public listeners: PluginListenerHandle[] = [];
    public opened = false;

    addListener(eventName: "browserFinished", listenerFunc: () => void) {
        const handle = new PluginListenerHandle(listenerFunc, (handle) => { this.testRemoveListener(handle); });
        this.listeners.push(handle);
        console.log('[MockPlugin: Browser]: Adding listener');
    }

    open(openOptions: any) {
        this.opened = true;
    }

    close() {
        this.testClose();
    }

    testReset() {
        this.opened = false;
        this.listeners = [];
    }

    testClose() {
        this.opened = false;
        console.log(`[MockPlugin: Browser]: Closed, executing ${this.listeners.length} listeners`);
        this.listeners.forEach((handle) => { handle.listenerFunc(); });
    }

    testRemoveListener(handle: PluginListenerHandle) {
        const index = this.listeners.indexOf(handle);
        if (index !== -1) {
            this.listeners.splice(index, 1);
        }
    }
}

export const Browser = new BrowserPlugin();
