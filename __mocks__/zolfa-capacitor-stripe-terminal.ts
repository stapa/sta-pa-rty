import { BehaviorSubject, Observable, Subject, filter, firstValueFrom, lastValueFrom, of } from "rxjs"

export interface Reader {
    stripeId?: string,
    locationId?: string,
    serialNumber?: string,
    simulated?: boolean,
    livemode?: boolean
}
export interface PaymentIntent {
    stripeId: string,
    currency: string,
    amount: number,
    status: number,
    metadata: any
}
export enum PaymentIntentStatus {
    RequiresPaymentMethod = 0,
    RequiresConfirmation = 1,
    RequiresCapture = 2,
    Processing = 3,
    Canceled = 4,
    Succeeded = 5
}
export enum DiscoveryMethod { 
    LocalMobile = 0
}
export interface PermissionStatus {
    location: 'prompt' | 'granted' | 'denied'
}
export class StripeTerminalPlugin {
    isInitialized = false;

    static _holdCompleteConnection$ = new BehaviorSubject(false);
    static _holdCompleteDiscovery$ = new BehaviorSubject(false);
    static _holdCompleteCollectPaymentMethods$ = new BehaviorSubject(false);

    private static _notifierCompleteConnection = StripeTerminalPlugin._holdCompleteConnection$.pipe(filter((hold) => !hold));
    private static _notifierCompleteDiscovery = StripeTerminalPlugin._holdCompleteDiscovery$.pipe(filter((hold) => !hold));
    private static _notifierCompleteCollectPaymentMethods = StripeTerminalPlugin._holdCompleteCollectPaymentMethods$.pipe(filter((hold) => !hold));
    static async checkPermissions(): Promise<PermissionStatus> {
        return { location: 'prompt' }
    }
    static async requestPermissions(): Promise<PermissionStatus> {
        return { location: 'granted' }
    }
    static async create(options: any) {
        return new StripeTerminalPlugin()
    }

    private currentPaymentIntent: null|PaymentIntent = null;

    discoverReaders(discoveryConfiguration: any): Observable<Reader[]> {
        this.isInitialized = true;
        if (discoveryConfiguration.discoveryMethod == DiscoveryMethod.LocalMobile) {
            return of([{}])
        } else {
            return of([])
        }
    }
    async connectLocalMobileReader(reader: Reader, options: any): Promise<Reader|null> {
        console.log('[Mock: StripeTerminalPlugin] connectLocalMobileReader() started');
        await firstValueFrom(StripeTerminalPlugin._notifierCompleteConnection);
        console.log('[Mock: StripeTerminalPlugin] connectLocalMobileReader() completed');
        return reader;
    }

    async retrievePaymentIntent(clientSecret: string): Promise<PaymentIntent|null> {
        console.log(`[Mock: StripeTerminalPlugin] retrivePaymentIntent(${clientSecret})`);
        this.currentPaymentIntent = {
            stripeId: 'pi_testPI',
            amount: 500,
            currency: '',
            status: 0,
            metadata: {}
        };
        return this.currentPaymentIntent;
    }

    async collectPaymentMethod(): Promise<PaymentIntent|null> {
        console.log('[Mock: StripeTerminalPlugin] collectPaymentMethod() started');
        await firstValueFrom(StripeTerminalPlugin._notifierCompleteCollectPaymentMethods);
        console.log('[Mock: StripeTerminalPlugin] collectPaymentMethod() completed');
        return this.currentPaymentIntent;
    }

    async processPayment(): Promise<PaymentIntent|null> {
        return this.currentPaymentIntent;
    }

    async disconnectReader(): Promise<void> {
        console.log('[Mock: StripeTerminalPlugin] reader disconnected');
        return;
    }

    async clearCachedCredentials() {
        console.log('[Mock: StripeTerminalPlugin] credentials cleared');
        return;
    }

    async getConnectedReader() {
        return null;
    }

}