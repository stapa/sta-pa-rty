#!/bin/bash

die () {
    set +x
    echo >&2 "$@"
    exit 1
}

[ "$#" -eq 1 ] || die "1 argument required, $# provided"
echo $1 | grep -E -q '^(production|staging)$' || die "argument must be either production or staging, $1 provided"

set -x
NODE_ENV=$1 npx ionic capacitor build android --configuration $1 --no-open || die "error during ionic execution"
./android/gradlew -p android assemble${1^} || die "error during gradle execution"
mkdir -p dist/
cp android/app/build/outputs/apk/$1/debug/app-$1-debug.apk dist/
set +x

# TODO: handle release apk signing
