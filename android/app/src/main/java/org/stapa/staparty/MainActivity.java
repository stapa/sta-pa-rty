package fr.stapa.staparty;

import android.webkit.CookieManager;
import com.getcapacitor.BridgeActivity;

public class MainActivity extends BridgeActivity {
    /* Needed for cookie persistence, see:
    https://github.com/ionic-team/capacitor/issues/3012#issuecomment-636017770
    TODO: remove once Native authentication will be cookie free
    */
    @Override
    public void onPause() {
        super.onPause();

        CookieManager.getInstance().flush();
    }
}
