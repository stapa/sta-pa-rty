import { enableProdMode } from '@angular/core';
import { bootstrapApplication } from '@angular/platform-browser';
import { RouteReuseStrategy, provideRouter } from '@angular/router';
import { provideHttpClient, withInterceptors } from '@angular/common/http';
import { provideIonicAngular, IonicRouteStrategy } from '@ionic/angular/standalone';

import { routes } from './app/app.routes';
import { AppComponent } from './app/app.component';
import { environment } from './environments/environment';
import { loggedOutInterceptor } from './app/interceptors/logged-out.interceptor';
import { timedOutInterceptor, GLOBAL_HTTP_TIMEOUT } from './app/interceptors/timed-out.interceptor';


if (environment.production) {
  enableProdMode();
}

bootstrapApplication(AppComponent, {
  providers: [
    { provide: GLOBAL_HTTP_TIMEOUT, useValue: 5000 },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    provideIonicAngular(),
    provideRouter(routes),
    provideHttpClient(
      withInterceptors([
        loggedOutInterceptor,
        timedOutInterceptor
      ])
    )
  ]
});
