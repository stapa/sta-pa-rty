import { Component, Input } from '@angular/core';
import { IonIcon } from '@ionic/angular/standalone';
import { addIcons } from 'ionicons';
import { hourglass } from 'ionicons/icons';

@Component({
  selector: 'app-explore-container',
  imports: [IonIcon],
  templateUrl: './explore-container.component.html',
  styleUrls: ['./explore-container.component.scss'],
  standalone: true,
})
export class ExploreContainerComponent {
  @Input() name?: string;
  
  constructor() {
    addIcons({ hourglass });
  }
}
