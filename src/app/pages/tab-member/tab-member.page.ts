import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonContent, IonHeader, IonTitle, IonToolbar } from '@ionic/angular/standalone';

import { ExploreContainerComponent } from '../../explore-container/explore-container.component';


@Component({
  selector: 'app-tab-member',
  templateUrl: 'tab-member.page.html',
  styleUrls: ['tab-member.page.scss'],
  standalone: true,
  imports: [
    CommonModule,
    IonTitle, IonToolbar, IonHeader, IonContent,
    ExploreContainerComponent
  ]
})
export class TabMemberPage {

  constructor() {}

}
