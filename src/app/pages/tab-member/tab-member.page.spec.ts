import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TabMemberPage } from './tab-member.page';

describe('TabMemberPage', () => {
  let component: TabMemberPage;
  let fixture: ComponentFixture<TabMemberPage>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [TabMemberPage]
    });
    fixture = TestBed.createComponent(TabMemberPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
