import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TabServePage } from './tab-serve.page';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { provideIonicAngular } from '@ionic/angular/standalone';

describe('TabServePage', () => {
  let component: TabServePage;
  let fixture: ComponentFixture<TabServePage>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideIonicAngular()],
      imports: [TabServePage, HttpClientTestingModule]
    });
    fixture = TestBed.createComponent(TabServePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
