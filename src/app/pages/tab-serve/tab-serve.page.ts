import { CommonModule } from '@angular/common';
import { Component, OnInit, inject, signal } from '@angular/core';
import { finalize } from 'rxjs';
import {
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
  IonCol,
  IonContent,
  IonHeader,
  IonRefresher,
  IonRefresherContent,
  IonRow,
  IonTitle,
  IonToolbar
} from '@ionic/angular/standalone';

import { ServeInputComponent } from 'src/app/components/serve-input/serve-input.component';
import { TopupButtonComponent } from '../../components/topup-button/topup-button.component';
import { BalanceButtonComponent } from '../../components/balance-button/balance-button.component';
import { TransactionService } from 'src/app/services/transaction/transaction.service';


@Component({
  selector: 'app-tab-serve',
  templateUrl: 'tab-serve.page.html',
  styleUrls: ['tab-serve.page.scss'],
  standalone: true,
  imports: [
    CommonModule,
    IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonContent, IonHeader, IonRefresher, IonRefresherContent, IonRow, IonTitle, IonToolbar,
    ServeInputComponent,
    TopupButtonComponent,
    BalanceButtonComponent,
  ]
})
export class TabServePage implements OnInit {
  private tx = inject(TransactionService);
  statistics = signal({number_free: 0, number_paid: 0, earnings: 0});
  handleRefresh(event: any) {
    this.tx.fetchStatistics().pipe(
      finalize(() => { event.target.complete(); })  
    ).subscribe((statistics) => { this.statistics.set(statistics); });
  }

  ngOnInit(): void {
    this.tx.fetchStatistics().subscribe(
      (statistics) => { this.statistics.set(statistics); }
    );
  }
}
