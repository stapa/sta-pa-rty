import { Component, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IonContent,
  IonHeader,
  IonIcon,
  IonItem,
  IonLabel,
  IonTitle,
  IonToolbar
} from '@ionic/angular/standalone';
import { addIcons } from 'ionicons';
import { logOut } from 'ionicons/icons';

import { AuthenticationService } from 'src/app/services/authentication/authentication.service';


@Component({
  selector: 'app-tab-settings',
  templateUrl: 'tab-settings.page.html',
  styleUrls: ['tab-settings.page.scss'],
  standalone: true,
  imports: [
    CommonModule,
    IonContent, IonHeader, IonIcon, IonItem, IonLabel, IonTitle, IonToolbar
  ],
})
export class TabSettingsPage {
  private authService = inject(AuthenticationService);

  constructor() {
    addIcons({ logOut });
  }

  logout() {
    console.log('[Tab: Settings] Logout started by user')
    this.authService.startLogout().subscribe(() => {
      console.log('[Tab: Settings] Logout completed');
    });
  }
}
