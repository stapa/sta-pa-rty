import { Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

export const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'serve',
        loadComponent: () =>
          import('../tab-serve/tab-serve.page').then((m) => m.TabServePage),
      },
      {
        path: 'member',
        loadComponent: () =>
          import('../tab-member/tab-member.page').then((m) => m.TabMemberPage),
      },
      {
        path: 'settings',
        loadComponent: () =>
          import('../tab-settings/tab-settings.page').then((m) => m.TabSettingsPage),
      },
      {
        path: '',
        redirectTo: '/tabs/serve',
        pathMatch: 'full',
      },
    ],
  },
  {
    path: '',
    redirectTo: '/tabs/serve',
    pathMatch: 'full',
  },
];
