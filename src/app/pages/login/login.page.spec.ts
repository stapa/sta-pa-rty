import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginPage } from './login.page';
import { click, findEl } from 'src/app/spec-helpers/element.spec-helper';
import { AuthenticationService, LoginConf } from 'src/app/services/authentication/authentication.service';
import { Subject, audit, of } from 'rxjs';
import { ApiService } from 'src/app/services/api/api.service';

describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;

  let fakeAuthenticationService: AuthenticationService;
  let fakeApiService: ApiService;
  let fakeBrowserClosed$!: Subject<void>;

  beforeEach(() => {
    fakeBrowserClosed$ = new Subject<void>();
    fakeAuthenticationService = jasmine.createSpyObj<AuthenticationService>(
      'AuthenticationService',
      { 
        startLogin: of(undefined).pipe(audit(() => fakeBrowserClosed$))
      },
      {
        loginConf$: of<LoginConf>({ qa_enabled: false })
      }
    )
    fakeApiService = jasmine.createSpyObj<ApiService>('ApiService', ['setDevFlags'])
    TestBed.configureTestingModule({
      imports: [LoginPage],
      providers: [
        { provide: AuthenticationService, useValue: fakeAuthenticationService },
        { provide: ApiService, useValue: fakeApiService }
      ]
    });
    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('renders without error', () => {
    expect(component).toBeTruthy();
  });

  describe('login button', () => {
    it('exists', () => {
      expect(findEl(fixture, 'login-button').nativeElement).toBeTruthy();
    });

    it('is not disabled', () => {
      expect(findEl(fixture, 'login-button').nativeElement.disabled).toBeFalsy(); 
    });

    it('calls service startLogin() on click', () => {
      click(fixture, 'login-button');
      expect(fakeAuthenticationService.startLogin).toHaveBeenCalledTimes(1);
    });

    it('is disabled after click', () => {
      click(fixture, 'login-button');
      fixture.detectChanges();
      expect(findEl(fixture, 'login-button').componentInstance.button.disabled).toBeTruthy();
    });

    it('is enabled after closing browser', () => {
      click(fixture, 'login-button');
      fakeBrowserClosed$.next();
      fixture.detectChanges();
      expect(findEl(fixture, 'login-button').nativeElement.disabled).toBeFalsy();
    });

    it('is enabled after service error', () => {
      click(fixture, 'login-button');
      fakeBrowserClosed$.error('Fake Error');
      fixture.detectChanges();
      expect(findEl(fixture, 'login-button').nativeElement.disabled).toBeFalsy();
    });

  })

});
