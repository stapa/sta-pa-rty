import { Component, computed, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import { finalize } from 'rxjs';
import { toSignal } from '@angular/core/rxjs-interop'
import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardTitle,
  IonCol,
  IonContent,
  IonIcon,
  IonLabel,
  IonRow
} from '@ionic/angular/standalone';

import { environment } from 'src/environments/environment';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { ApiService } from 'src/app/services/api/api.service';
import { SpinnerButtonComponent } from 'src/app/components/spinner-button/spinner-button.component';



@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
  standalone: true,
  imports: [
    CommonModule,
    IonButton, IonCard, IonCardContent, IonCardHeader, IonCardTitle, IonCol, IonContent, IonIcon, IonLabel, IonRow,
    SpinnerButtonComponent
  ]
})
export class LoginPage {
  private authService = inject(AuthenticationService);
  private api = inject(ApiService);
  public production = environment.production;
  readonly loginLoading = signal(false);
  private readonly loginConf = toSignal(this.authService.loginConf$);
  readonly qaEnabled = computed(() => this.loginConf()?.qa_enabled);

  loginQa() {
    this.api.setDevFlags({useQaBackend: true});
    this.login();
  }

  loginPasteur() {
    this.api.setDevFlags({useQaBackend: false});
    this.login();
  }

  private login() {
    console.log('[Page: Login] Login started by user')
    this.loginLoading.set(true);
    this.authService.startLogin().pipe(
      finalize(() => {
          this.loginLoading.set(false);
      })
    ).subscribe({
      next: () => { console.log('[Page: Login] Login finished'); },
      error: (err) => { console.log('[Page: Login] Error in AuthenticationService'); }
    });
  }
}
