import { HttpErrorResponse, HttpHandlerFn, HttpRequest } from "@angular/common/http";
import { inject } from "@angular/core";
import { AuthenticationService } from "../services/authentication/authentication.service";
import { catchError } from "rxjs";

export function loggedOutInterceptor(request: HttpRequest<unknown>, next: HttpHandlerFn) {
    const authService = inject(AuthenticationService);
    return next(request).pipe(
        catchError((err: HttpErrorResponse) => {
            if (err.status == 401) {
                console.log("[Interceptor: loggedOut] Caught 401, signalling expired or failed login.");
                authService.handleLogout();
            }
            throw err;
        })
    )
}
