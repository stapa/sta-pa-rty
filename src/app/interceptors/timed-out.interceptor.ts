import { HttpHandlerFn, HttpRequest } from "@angular/common/http";
import { InjectionToken, inject } from "@angular/core";
import { timeout } from "rxjs";

export const GLOBAL_HTTP_TIMEOUT = new InjectionToken<number>('globalHttpTimeout');

export function timedOutInterceptor(request: HttpRequest<unknown>, next: HttpHandlerFn) {
    const timeoutMillis = inject(GLOBAL_HTTP_TIMEOUT);
    return next(request).pipe(timeout(timeoutMillis));
}
