import { Routes } from '@angular/router';
import { loggedInGuard, notLoggedInGuard, loginCallbackGuard } from './guards/authentication.guard';

export const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./pages/tabs/tabs.routes').then((m) => m.routes),
    canActivate: [ loggedInGuard ]
  },
  {
    path: 'login',
    loadComponent: () => import('./pages/login/login.page').then( m => m.LoginPage),
    canActivate: [ notLoggedInGuard ]
  },
  {
    // only involves non-native login callback
    path: 'loginCallback',
    canActivate: [ loginCallbackGuard ],
    children: []
  }
];
