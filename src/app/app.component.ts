import { Component, EnvironmentInjector, NgZone, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonApp, IonRouterOutlet } from '@ionic/angular/standalone';
import { App, URLOpenListenerEvent } from '@capacitor/app';

import { AuthenticationService } from './services/authentication/authentication.service';
import { environment } from '../environments/environment';
import { ApiService } from './services/api/api.service';
import { TelemetryService } from './services/telemetry/telemetry.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  standalone: true,
  imports: [
    CommonModule,
    IonApp, IonRouterOutlet
  ],
})
export class AppComponent {
  public environmentInjector = inject(EnvironmentInjector);
  private zone = inject(NgZone);
  private telemetry = inject(TelemetryService);

  constructor() {
    this.initializeApp();
  }

  initializeApp() {
    this.telemetry.start();
    // native only: app called with deeplink
    App.addListener('appUrlOpen', (event) => {
      this.zone.run(() => { this.handleDeepLink(event);})
    });
  } 

  handleDeepLink(event: URLOpenListenerEvent) {
    const slug = event.url.split(environment.appUrl).pop();
    console.log('[App] Handling DeepLink: ' + event.url);
    switch(slug) {
      // return callback from backend authentication flow
      case '/loginCallback': {
        const authService = this.environmentInjector.get(AuthenticationService);
        authService.handleCallback();
        break;
      }
      case '/devFlags/useQaBackend/on': {
        const api = this.environmentInjector.get(ApiService);
        api.setDevFlags({ useQaBackend: true })
        break;
      }
      case '/devFlags/useQaBackend/off': {
        const api = this.environmentInjector.get(ApiService);
        api.setDevFlags({ useQaBackend: false })
        break;
      }
      default: {
        console.log('[App] Unknown DeepLink, doing nothing');
        break;
      }
    }
  }
}
