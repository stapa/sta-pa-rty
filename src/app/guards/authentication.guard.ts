import { inject } from '@angular/core';
import { Router } from '@angular/router';
import { filter, map, of } from 'rxjs';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { toObservable } from '@angular/core/rxjs-interop';

export function loggedInGuard() {
  const router = inject(Router);
  const authService = inject(AuthenticationService);
  return toObservable(authService.loggedIn).pipe(
    // wait for loggedIn to be defined before resolving
    filter((loggedIn) => (loggedIn !== undefined)),
    // redirect if not loggedIn
    map((loggedIn) => (loggedIn ? true : router.parseUrl('/login')))
  );
}

export function notLoggedInGuard() {
  const router = inject(Router);
  const authService = inject(AuthenticationService);
  return toObservable(authService.loggedIn).pipe(
    // wait for loggedIn to be defined before resolving
    filter((loggedIn) => (loggedIn !== undefined)),
    // redirect if loggedIn
    map((loggedIn) => (loggedIn ? router.parseUrl('/') : true))
  );
}

export function loginCallbackGuard() {
  console.log("[Guard: loginCallback] Route /loginCallback hit")
  const authSerivce = inject(AuthenticationService);
  authSerivce.handleCallback();
  return of(true);
}
