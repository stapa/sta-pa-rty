import {
    InstrumentationBase,
    InstrumentationConfig,
} from '@opentelemetry/instrumentation';
import {
    SpanStatusCode,
    context,
    Span,
    SpanKind,
    Exception
} from '@opentelemetry/api';
import { Observable, finalize, tap } from 'rxjs';
import {
    StripeTerminalPlugin,
    PaymentIntent,
    Reader,
    PermissionStatus
} from 'zolfa-capacitor-stripe-terminal';

export class StripeTerminalPluginInstrumentation extends InstrumentationBase<StripeTerminalPlugin> {
    constructor(config?: InstrumentationConfig) {
        super('StripeTerminalPluginInstrumentor', '0.3', config);
    }

    init() { };

    override enable(): void {
        this._wrap(
            StripeTerminalPlugin, 'create',
            this.patchAsyncFn<StripeTerminalPlugin>('create')
        );
        this._wrap(
            StripeTerminalPlugin, 'checkPermissions',
            this.patchAsyncFn<PermissionStatus>('checkPermissions')
        );
        this._wrap(
            StripeTerminalPlugin, 'requestPermissions',
            this.patchAsyncFn<PermissionStatus>('requestPermissions')
        );
        this._wrap(
            StripeTerminalPlugin.prototype, 'retrievePaymentIntent',
            this.patchAsyncMethod<PaymentIntent|null>('retrievePaymentIntent', this.enrichSpanWithPI)
        );
        this._wrap(
            StripeTerminalPlugin.prototype, 'collectPaymentMethod',
            this.patchAsyncMethod<PaymentIntent|null>('collectPaymentMethod', this.enrichSpanWithPI)
        );
        this._wrap(
            StripeTerminalPlugin.prototype, 'processPayment',
            this.patchAsyncMethod<PaymentIntent|null>('processPayment', this.enrichSpanWithPI)
        );
        this._wrap(
            StripeTerminalPlugin.prototype, 'discoverReaders',
            this.patchObservableMethod<Reader[]>('discoverReaders')
        );
        this._wrap(
            StripeTerminalPlugin.prototype, 'connectLocalMobileReader', 
            this.patchAsyncMethod<Reader|null>('connectLocalMobileReader', this.enrichSpanWithReader)
        );
    }

    private createSpan(name: string): Span {
        const span = this.tracer.startSpan(name, { kind: SpanKind.CLIENT });
        const parentContext = context.active();
        return span;
    }

    private recordException(span: Span, error: Exception) {
        span.recordException(error);
        span.setStatus({ code: SpanStatusCode.ERROR });
    }

    private enrichSpanWithPI(span: Span, pI: PaymentIntent|null) {
        if (!pI) {
            throw Error('Plugin returned empty PaymentIntent.')
        }
        span.setAttributes({
            'stripe.payment_intent.id': pI.stripeId,
            'stripe.payment_intent.amount': pI.amount,
            'stripe.payment_intent.currency': pI.currency,
            'stripe.payment_intent.status': pI.status,
            'stripe.payment_intent.metadata': pI.metadata ? JSON.stringify(pI.metadata) : undefined
        });
        return pI;
    }
    
    private enrichSpanWithReader(span: Span, reader: Reader|null) {
        console.log('enriching reader span');
        if (!reader) {
            throw Error('Plugin returned empty reader list.')
        }
        console.log(`reader connected, id: ${reader.stripeId}`);
        span.setAttributes({
            'stripe.reader.id': reader.stripeId ? reader.stripeId : undefined,
            'stripe.reader.location_id': reader.locationId ? reader.locationId : undefined,
            'stripe.reader.serial_number': reader.serialNumber ? reader.serialNumber : undefined,
            'stripe.reader.simulated': reader.simulated ? reader.simulated : undefined,
            'stripe.reader.live_mode': reader.livemode ? reader.livemode : undefined
        });
        return reader;
    }

    private patchObservableMethod<T>(name: string) {
        return (original: ((...args: any[]) => Observable<T>)) => {
            const instrumentation = this;
            return function (this: StripeTerminalPlugin, ...args: any[]): Observable<T> {
                const span = instrumentation.createSpan(name);
                return original.apply(this, args).pipe(
                    tap({ error: (error) => { instrumentation.recordException(span, error)} }),
                    finalize(() => { span.end(); })
                );
            };
        };
    }

    private patchAsyncFn<T>(name: string) {
        return (original: (...args: any[]) => Promise<T>) => {
            const instrumentation = this;
            return async function (...args: any[]): Promise<T> {
                const span = instrumentation.createSpan(name);
                try {
                    let result = await original(...args);
                    span.end();
                    return result;
                } catch(error: any) {
                    instrumentation.recordException(span, error);
                    span.end();
                    throw error;
                }
            }
        }
    }

    private patchAsyncMethod<T>(
        name: string,
        interceptor?: ((span: Span, input: Awaited<T>) => Awaited<T>)
    ) {
        return (original: ((...args: any[]) => Promise<T>)) => {
            const instrumentation = this;
            return async function (this: StripeTerminalPlugin, ...args: any[]): Promise<T> {
                const span = instrumentation.createSpan(name)
                try { 
                    let result = await original.apply(this, args);
                    if (interceptor) {
                        result = interceptor(span, result);
                    }
                    span.end();
                    return result;
                } catch(error: any) {
                    instrumentation.recordException(span, error);
                    span.end();
                    throw error;
                }
            };
        };
    }
}
