import { CommonModule } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  NgZone,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  inject
} from '@angular/core';
import {
  IonButton,
  IonButtons,
  IonContent,
  IonFab,
  IonFabButton,
  IonHeader,
  IonIcon,
  IonTitle,
  IonToolbar
} from '@ionic/angular/standalone';
import { addIcons } from 'ionicons';
import { flashlight, close } from 'ionicons/icons';
import { Capacitor } from '@capacitor/core';
import { BarcodeFormat, BarcodeScanner, LensFacing, StartScanOptions } from '@capacitor-mlkit/barcode-scanning';
import { Html5QrcodeScanType, Html5QrcodeScanner } from 'html5-qrcode';


// Inspired from https://github.com/robingenz/capacitor-mlkit-plugin-demo
@Component({
  selector: 'app-qrcode-scanner',
  standalone: true,
  imports: [
    CommonModule,
    IonButton, IonButtons, IonContent, IonFab, IonFabButton, IonHeader, IonIcon, IonTitle, IonToolbar
  ],
  templateUrl: './qrcode-scanner.component.html',
  styleUrls: ['./qrcode-scanner.component.scss'],
})
export class QrcodeScannerComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() public stateMessage!: string;
  @Output() onCancel = new EventEmitter<null>();
  @Output() onScanned = new EventEmitter<string>();

  private zone = inject(NgZone);
  
  @ViewChild('square') public squareElement: ElementRef<HTMLDivElement> | undefined;
  private webScanner!: Html5QrcodeScanner;
  public isTorchAvailable = false;
  public isNative = Capacitor.isNativePlatform();

  constructor() {
    addIcons({ flashlight, close });
  }

  ngOnInit() {
    if (this.isNative) {
      BarcodeScanner.isTorchAvailable().then((result) => {
        this.isTorchAvailable = result.available;
      });
    } else {
      this.isTorchAvailable = false;
    }
  }

  ngAfterViewInit() {
    if (this.isNative) {
      this.startScan();
    } else {
      this.startScanWeb();
    }
  }

  ngOnDestroy() {
    if (this.isNative) {
      console.log('[Component: QrCodeScanner] Stopping scanner (native)');
      this.stopScan();
    } else {
      console.log('[Component: QrCodeScanner] Stopping scanner (web)');
      this.stopScanWeb();
    }
  }

  cancel() {
    console.log('[Component: QrCodeScanner] User canceled scan');
    this.onCancel.emit();
  }

  async toggleTorch() {
    await BarcodeScanner.toggleTorch();
  }

  private async startScan() {
    console.log('[Component: QrCodeScanner] Starting scanner (native)');
    document.querySelector('body')?.classList.add('barcode-scanning-active');
    const options: StartScanOptions = {
      formats: [ BarcodeFormat.QrCode ],
      lensFacing: LensFacing.Back
    };
    const squareElementBoundingClientRect =
      this.squareElement?.nativeElement.getBoundingClientRect();
    const scaledRect = squareElementBoundingClientRect
      ? {
        left: squareElementBoundingClientRect.left * window.devicePixelRatio,
        right: squareElementBoundingClientRect.right * window.devicePixelRatio,
        top: squareElementBoundingClientRect.top * window.devicePixelRatio,
        bottom: squareElementBoundingClientRect.bottom * window.devicePixelRatio,
        width: squareElementBoundingClientRect.width * window.devicePixelRatio,
        height: squareElementBoundingClientRect.height * window.devicePixelRatio
      }
      : undefined;
    const detectionCornerPoints = scaledRect
      ? [
        [scaledRect.left, scaledRect.top],
        [scaledRect.left + scaledRect.width, scaledRect.top],
        [scaledRect.left + scaledRect.width, scaledRect.top + scaledRect.height],
        [scaledRect.left, scaledRect.top + scaledRect.height]
      ]
      : undefined;
    const listener = await BarcodeScanner.addListener(
      'barcodeScanned',
      async (event) => {
        this.zone.run(() => {
          const cornerPoints = event.barcode.cornerPoints;
          /* if (detectionCornerPoints && cornerPoints) {
            if (
              detectionCornerPoints[0][0] > cornerPoints[0][0] ||
              detectionCornerPoints[0][1] > cornerPoints[0][1] ||
              detectionCornerPoints[1][0] < cornerPoints[1][0] ||
              detectionCornerPoints[1][1] > cornerPoints[1][1] ||
              detectionCornerPoints[2][0] < cornerPoints[2][0] ||
              detectionCornerPoints[2][1] < cornerPoints[2][1] ||
              detectionCornerPoints[3][0] > cornerPoints[3][0] ||
              detectionCornerPoints[3][1] < cornerPoints[3][1]
            ) {
              return;
            }
          } */
          listener.remove();
          console.log('[Component: QrCodeScanner] Scanned (native): ' + event.barcode.rawValue);
          this.onScanned.emit(event.barcode.rawValue);
        });
      }
    );
    await BarcodeScanner.startScan(options); 
    console.log('[Component: QrCodeScanner] Scanner started (native)');
  }

  private async stopScan() {
    console.log('[Component: QrCodeScanner] Stopping scanner (native)');
    document.querySelector('body')?.classList.remove('barcode-scanning-active');
    await BarcodeScanner.stopScan();
    console.log('[Component: QrCodeScanner] Scanner stopped (native)');
  }

  private async startScanWeb() {
    console.log('[Component: QrCodeScanner] Starting scanner (web)');
    this.webScanner = new Html5QrcodeScanner(
      "reader",
      {
        fps: 10,
        qrbox: 250,
        supportedScanTypes: [ Html5QrcodeScanType.SCAN_TYPE_CAMERA ]
      },
      false
    )
    this.webScanner.render(
      (decodedText, decodedResult) => {
        console.log('[Component: QrCodeSanner] Scanned (web): ' + decodedText);
        this.webScanner.pause();
        this.onScanned.emit(decodedText);
      },
      undefined
    )
  }

  private async stopScanWeb() {
    console.log('[Component: QrCodeScanner] Stopping scanner (web)');
    await this.webScanner.clear();
    console.log('[Component: QrCodeScanner] Scanner stopped (web)');
  }
}
