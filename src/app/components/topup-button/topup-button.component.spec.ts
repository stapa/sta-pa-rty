import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TopupButtonComponent } from './topup-button.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ModalController, provideIonicAngular } from '@ionic/angular/standalone';
import { IonicModule } from '@ionic/angular';

describe('TopupButtonComponent', () => {
  let component: TopupButtonComponent;
  let fixture: ComponentFixture<TopupButtonComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideIonicAngular()],
      imports: [TopupButtonComponent, HttpClientTestingModule]
    });
    fixture = TestBed.createComponent(TopupButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
