import { Component, OnDestroy, OnInit, ViewChild, computed, inject } from '@angular/core';
import { Observable } from 'rxjs';
import { CommonModule } from '@angular/common';
import {
  ActionSheetButton,
  IonActionSheet,
  IonButton,
  IonIcon,
  IonLabel,
  IonModal,
  ModalController
} from '@ionic/angular/standalone';
import { addIcons } from 'ionicons';
import { card } from 'ionicons/icons';

import { StripeTerminalService } from '../../services/stripe-terminal/stripe-terminal.service';
import { MemberPickerComponent } from '../member-picker/member-picker.component';
import { Member } from '../../services/member/member.service';
import { SpinnerButtonComponent } from '../spinner-button/spinner-button.component';


@Component({
  selector: 'app-topup-button',
  standalone: true,
  imports: [
    CommonModule,
    IonActionSheet, IonButton, IonIcon, IonLabel, IonModal,
    MemberPickerComponent,
    SpinnerButtonComponent
  ],
  templateUrl: './topup-button.component.html',
  styleUrls: ['./topup-button.component.scss']
})
export class TopupButtonComponent implements OnInit, OnDestroy {
  private paymentService = inject(StripeTerminalService);
  private modalCtrl = inject(ModalController);
  @ViewChild('pickerModal') pickerModal!: IonModal;
  selectedAmount: number|undefined;
  currentPayment!: Observable<string>;  

  terminalReady = computed(() => this.paymentService.state() == "standby");
  terminalBusy = computed(() => {
    switch(this.paymentService.state()) {
      case 'standby':
        return false;
      case 'disconnected':
        return false;
      default:
        return true;
    }
  });

  topUpButtons: ActionSheetButton[] = [
    ...this.formatTopUpButtons([ 500, 1000, 1500, 2000 ]),  // cents of €
    { text: 'Cancel', role: 'cancel' }
  ];

  constructor() {
    addIcons({ card });
  }

  ngOnInit() {
    console.log('[Component: TopUpButton] init');
    this.paymentService.connect();
  }

  ngOnDestroy() {
    console.log('[Component: TopUpButton] destroy');
  }

  private formatTopUpButtons(amounts: number[]): ActionSheetButton[] {
    return amounts.map((amount) => ({
      text: `€ ${(amount/100.0).toFixed(2)}`,
      handler: () => {
        this.topUp(amount);
      }
    }));
  }
  
  onPickerDismiss() {
    this.selectedAmount = undefined;
  }

  topUp(amount: number) {
    this.pickerModal.present();
    this.selectedAmount = amount;
  }

  onMemberCancel() {
    this.pickerModal.dismiss();
  }

  onMemberPick(member: Member) {
    if (this.selectedAmount) {
      try {
        this.paymentService.startPayment({
          member_id: member.id,
          amount: this.selectedAmount,
          reason: 'topup'
        }).subscribe({
          next: () => { window.alert('Success!'); },
          error: (error) => { window.alert(`Failed! ${error}`); }
        });
      } catch {
        window.alert('ERROR: Terminal not ready!')
      }
    } else {
      console.error('[Component: TopupButton] Unexpected error: selectedAmount undefined');
    }
    this.pickerModal.dismiss();
    this.selectedAmount = undefined;
  }

}
