import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SwipeVerifyComponent } from './swipe-verify.component';

describe('SwipeVerifyComponent', () => {
  let component: SwipeVerifyComponent;
  let fixture: ComponentFixture<SwipeVerifyComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [SwipeVerifyComponent]
    });
    fixture = TestBed.createComponent(SwipeVerifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
