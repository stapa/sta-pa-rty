import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BeerCounterComponent } from './beer-counter.component';

describe('BeerCounterComponent', () => {
  let component: BeerCounterComponent;
  let fixture: ComponentFixture<BeerCounterComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [BeerCounterComponent]
    });
    fixture = TestBed.createComponent(BeerCounterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  
});
