import { Component, EventEmitter, Input, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonIcon } from '@ionic/angular/standalone';
import { NgxMatNumberInputSpinnerModule } from 'ngx-mat-number-input-spinner';
import { addIcons } from 'ionicons';
import { add, remove } from 'ionicons/icons'


@Component({
  selector: 'app-beer-counter',
  standalone: true,
  imports: [
    CommonModule,
    IonIcon,
    NgxMatNumberInputSpinnerModule,
  ],
  templateUrl: './beer-counter.component.html',
  styleUrls: ['./beer-counter.component.scss']
})
export class BeerCounterComponent {
  @Input() count: number = 1;
  @Output() countChange = new EventEmitter<number>();

  constructor() {
    addIcons({ add, remove });
  }

  onCounterChange(event: any) {
    // parseInt required not to make tx fails because event.target.value: any is not int
    // TODO: create test case for thi situation
    this.count = parseInt(event.target.value);
    this.countChange.emit(this.count);
  }
}
