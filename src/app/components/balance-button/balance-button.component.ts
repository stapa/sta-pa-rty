import { Component, ViewChild, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IonButton,
  IonIcon,
  IonLabel,
  IonModal,
  ModalController
} from '@ionic/angular/standalone';
import { addIcons } from 'ionicons';
import { card } from 'ionicons/icons';

import { Member } from '../../services/member/member.service';
import { MemberPickerComponent } from '../member-picker/member-picker.component';


@Component({
  selector: 'app-balance-button',
  standalone: true,
  imports: [
    CommonModule,
    IonButton, IonIcon, IonLabel, IonModal,
    MemberPickerComponent
  ],
  templateUrl: './balance-button.component.html',
  styleUrls: ['./balance-button.component.scss']
})
export class BalanceButtonComponent {
  @ViewChild('pickerModal') pickerModal!: IonModal;

  constructor() {
    addIcons({ card });
  }

  onMemberCancel() {
    this.pickerModal.dismiss();
  }

  onMemberPick(member: Member) {
    window.alert(`User: ${member.display_name}\nCurrent balance: € ${(member.balance/100).toFixed(2)}`);
    this.pickerModal.dismiss();
  }
}
