import { Component, ViewChild, inject, signal } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IonButton,
  IonCardContent,
  IonIcon,
  IonLabel,
  IonModal,
  IonToolbar,
  ModalController
} from '@ionic/angular/standalone';
import { addIcons } from 'ionicons';
import { beer } from 'ionicons/icons';

import { ServeResultComponent } from '../serve-result/serve-result.component';
import { BeerCounterComponent } from '../beer-counter/beer-counter.component';
import { MemberPickerComponent } from '../member-picker/member-picker.component';
import { TransactionResult, TransactionService } from 'src/app/services/transaction/transaction.service';
import { Member } from 'src/app/services/member/member.service';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { catchError } from 'rxjs';
import { ErrorNotifierService } from 'src/app/services/error-notifier/error-notifier.service';

@Component({
  selector: 'app-serve-input',
  standalone: true,
  imports: [
    CommonModule,
    IonButton, IonCardContent, IonIcon, IonLabel, IonModal, IonToolbar, BeerCounterComponent, MemberPickerComponent
  ],
  templateUrl: './serve-input.component.html',
  styleUrls: ['./serve-input.component.scss']
})
export class ServeInputComponent {
  @ViewChild('serveModal') public serveModal!: IonModal;
  public beerCount: number = 1;

  private txService = inject(TransactionService);
  private loadService = inject(LoadingService);
  private modalCtrl = inject(ModalController);
  private errorServ = inject(ErrorNotifierService);
  
  serveLoading = signal(false);

  constructor() {
    addIcons({ beer });
  }

  resetBeerCounter() {
    this.beerCount = 1;
  }

  serve(member: Member) {
    const beers = this.beerCount;
    console.log(`[Tab: Serve] Serving ${beers} beers to '${member.display_name}'`);
    this.serveModal.dismiss();
    this.resetBeerCounter();;
    const tx = this.txService.prepareTxReq(member, beers);
    this.txService.executeIfEnoughFunds(tx)
      .pipe(
        catchError((err) => {
          console.log(`[Component: ServeResut]: serve failed with error: ${err}`);
          this.errorServ.notifyError(err);
          throw err;
        }),
        this.loadService.trackLoading('Checking balance...')
      )
      .subscribe((txResult) => { this.displayServeResult(member, txResult); })
  }

  async displayServeResult(member: Member, txResult: TransactionResult) {
    const serveResultModal = await this.modalCtrl.create({
      component: ServeResultComponent,
      componentProps: {
        member: member,
        txResult: txResult
      }
    });
    await serveResultModal.present();
  }
  serveStateMessage() {
    return `Serving ${this.beerCount} beer${(this.beerCount > 1) ? 's' : ''}`;
  }


  cancelServe() {
    console.log('[Tab: Serve] Serve canceled');
    this.serveModal.dismiss();
    this.resetBeerCounter();
  }

}
