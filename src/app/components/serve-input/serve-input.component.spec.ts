import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServeInputComponent } from './serve-input.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ModalController, provideIonicAngular } from '@ionic/angular/standalone';

describe('ServeInputComponent', () => {
  let component: ServeInputComponent;
  let fixture: ComponentFixture<ServeInputComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideIonicAngular()],
      imports: [ServeInputComponent, HttpClientTestingModule]
    });
    fixture = TestBed.createComponent(ServeInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
