import { Component, Input, OnInit, inject } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  IonButton,
  IonButtons,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonHeader,
  IonIcon,
  IonTitle,
  IonToolbar,
  ModalController
} from '@ionic/angular/standalone';
import { addIcons } from 'ionicons';
import { checkmarkDoneCircle, closeCircle, close } from 'ionicons/icons';
import { Haptics, NotificationType } from '@capacitor/haptics';
import { NativeAudio } from '@awesome-cordova-plugins/native-audio/ngx';
import { Capacitor } from '@capacitor/core';

import { TransactionResult, TransactionService } from 'src/app/services/transaction/transaction.service';
import { Member } from 'src/app/services/member/member.service';
import { SwipeVerifyComponent } from '../swipe-verify/swipe-verify.component';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { catchError } from 'rxjs';
import { ErrorNotifierService } from 'src/app/services/error-notifier/error-notifier.service';


const isNative = Capacitor.isNativePlatform();

@Component({
  selector: 'app-serve-result',
  standalone: true,
  imports: [
    CommonModule,
    IonButton, IonButtons, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonHeader, IonIcon, IonTitle, IonToolbar,
    SwipeVerifyComponent
  ],
  templateUrl: './serve-result.component.html',
  styleUrls: ['./serve-result.component.scss']
})
export class ServeResultComponent implements OnInit {
  @Input({ required: true }) txResult!: TransactionResult;
  @Input({ required: true }) member: Member = { id: 0, display_name: 'MISSING', balance: 0 };
  private txService = inject(TransactionService);
  private nativeAudio = new NativeAudio();
  private loadService = inject(LoadingService);
  private errorServ = inject(ErrorNotifierService);

  lineDesc = {
    "beer": "Paid beer(s)",
    "free_beer": "Offered welcome beer"
  }

  private modalCtrl = inject(ModalController);

  constructor() {
    addIcons({ checkmarkDoneCircle, closeCircle, close});
  }

  ngOnInit() {
    this.onTxChange();
  }

  onTxChange() {
      if (this.txResult.executed) {
        this.onSuccess();
      } else {
        this.onPaymentRequired();
      }
  }

  async onSuccess() {
    Haptics.notification({ type: NotificationType.Success });
    if (isNative) {
      await this.nativeAudio.preloadSimple('success', 'success.mp3');
      await this.nativeAudio.play('success', () => { this.nativeAudio.unload('success') });
    };
  }

  async onPaymentRequired() {
    Haptics.vibrate({ duration: 500 });
    if (isNative) {
      await this.nativeAudio.preloadSimple('payment_required', 'payment_required.mp3');
      await this.nativeAudio.play('payment_required', () => { this.nativeAudio.unload('payment_required') });
    }
  }

  cancel() {
    this.modalCtrl.dismiss();
  }

  onCashPayment() {
    console.log("[Page: ServeResult] Cash payment confirmed on UI");
    
    const cashIn = -this.txResult.balance_after;
    const beerCount = this.txResult.lines
      .filter((line) => line.type == "beer" || line.type == "free_beer")
      .map((line) => line.quantity)
      .reduce((sum, current) => sum += current, 0);
    const newTx = this.txService.prepareTxReq(this.member, beerCount, cashIn);
    this.txService.executeIfEnoughFunds(newTx)
      .pipe(
        this.loadService.trackLoading('Registering payment...'),
        catchError((err) => {
          console.log(`[Component: ServeResut]: cash payment failed with error: ${err}`);
          this.errorServ.notifyError(err);
          window.alert('ERROR: No operation executed!');
          this.cancel();
          throw err;
        })
      )
      .subscribe((result) => {
        this.txResult = result;
        this.onTxChange();
      });
  }
}
