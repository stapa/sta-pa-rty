import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServeResultComponent } from './serve-result.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { provideIonicAngular } from '@ionic/angular/standalone';

describe('ServeResultComponent', () => {
  let component: ServeResultComponent;
  let fixture: ComponentFixture<ServeResultComponent>;

  const mockTx = {
    empty: {
      member: { id: 1, display_name: "Lorenzo Z" },
      balance_before: 0,
      balance_after: 0,
      lines: [],
      executed: true
    }
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [provideIonicAngular()],
      imports: [ServeResultComponent, HttpClientTestingModule]
    });
    fixture = TestBed.createComponent(ServeResultComponent);
    component = fixture.componentInstance;
  });

  it('should create with empty tx', () => {
    component.txResult = mockTx.empty;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
});
