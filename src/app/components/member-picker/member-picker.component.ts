import { CommonModule } from '@angular/common';
import { Component, EventEmitter, Input, Output, inject } from '@angular/core';
import {
  IonFab,
  IonFabButton,
  IonIcon,
  IonModal
} from '@ionic/angular/standalone';
import { addIcons } from 'ionicons';
import { search } from 'ionicons/icons';

import { MemberSearchComponent } from '../member-search/member-search.component';
import { QrcodeScannerComponent } from '../qrcode-scanner/qrcode-scanner.component';
import { Member, MemberService } from '../../services/member/member.service';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { catchError } from 'rxjs';
import { ErrorNotifierService } from 'src/app/services/error-notifier/error-notifier.service';


@Component({
  selector: 'app-member-picker',
  standalone: true,
  imports: [
    CommonModule,
    IonFab, IonFabButton, IonIcon, IonModal,
    MemberSearchComponent,
    QrcodeScannerComponent
  ],
  templateUrl: './member-picker.component.html',
  styleUrls: ['./member-picker.component.scss'],
})
export class MemberPickerComponent {
  @Input() stateMessage!: string;
  @Output() onCancel = new EventEmitter<null>();
  @Output() onPick = new EventEmitter<Member>();

  private memberService = inject(MemberService);
  private loadService = inject(LoadingService);
  private errorServ = inject(ErrorNotifierService);

  currentMode: "qrcode"|"search" = "qrcode";

  constructor() {
    addIcons({ search });
  }

  fromQrcode(qrcode: string) {
    console.log(`[Component: MemberPicker] Searching member with QrCode: '${qrcode}'`)
    this.memberService.fromQrcode(qrcode)
      .pipe(
        this.loadService.trackLoading('Searching QR code...'),
        catchError((err) => {
          console.log(`[Component: MemberSearch] MemberService returned error on search: ${err}`);
          this.errorServ.notifyError(err);
          this.cancel();
          throw err;
        })
      )
      .subscribe((member) => {
        if (member) {
          console.log(`[Component: MemberPicker] Matching member found and picked: '${member.display_name}'`)
          this.onPick.emit(member);
        } else {
          this.onUnregisteredBadge(qrcode);
        }
      });
  }

  onUnregisteredBadge(qrcode: string) {
    console.log(`[Component: MemberPicker] Unregistered QrCode: '${qrcode}'`);
    // for DEBUG
    window.alert(`Unregistered QrCode: '${qrcode}' - Not A Member!`);
    this.onCancel.emit();
  } 

  returnMember(member: Member) {
    this.onPick.emit(member);
  }

  cancel() {
    this.onCancel.emit();
  }
}
