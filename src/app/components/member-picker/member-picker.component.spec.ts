import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing'
import { MemberPickerComponent } from './member-picker.component';

describe('MemberPickerComponent', () => {
  let component: MemberPickerComponent;
  let fixture: ComponentFixture<MemberPickerComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MemberPickerComponent, HttpClientTestingModule]
    });
    fixture = TestBed.createComponent(MemberPickerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
