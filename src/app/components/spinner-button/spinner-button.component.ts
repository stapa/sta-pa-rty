import { CommonModule } from '@angular/common';
import { Component, ViewChild, input, effect } from '@angular/core';
import { IonButton, IonSpinner } from '@ionic/angular/standalone';

@Component({
  selector: 'app-spinner-button',
  standalone: true,
  imports: [CommonModule, IonButton, IonSpinner],
  templateUrl: './spinner-button.component.html',
  styleUrl: './spinner-button.component.scss',
})
export class SpinnerButtonComponent {
  @ViewChild(IonButton, { static: true }) button!: IonButton;
  loading = input<boolean>();
  disabled = input<boolean>(false);
  color = input<any>('primary');

  constructor() {
    effect(() => { this.button.disabled = this.disabled(); });
    effect(() => { this.button.color = this.color(); });
  }
  
}



