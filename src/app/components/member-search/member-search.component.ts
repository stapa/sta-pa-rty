import { CommonModule } from '@angular/common';
import {
  AfterViewInit,
  Component,
  EventEmitter,
  Input,
  Output,
  ViewChild,
  inject
} from '@angular/core';
import { Observable, catchError, of } from 'rxjs';
import {
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonList,
  IonSearchbar,
  IonSpinner,
  IonTitle,
  IonToolbar
} from '@ionic/angular/standalone';

import { MemberService, Member } from '../../services/member/member.service';
import { ErrorNotifierService } from 'src/app/services/error-notifier/error-notifier.service';


@Component({
  selector: 'app-member-search',

  standalone: true,
  imports: [
    CommonModule,
    IonContent, IonHeader, IonItem, IonLabel, IonList, IonSearchbar, IonSpinner, IonTitle, IonToolbar
  ],
  templateUrl: './member-search.component.html',
  styleUrls: ['./member-search.component.scss'],
  host: { 'class': 'ion-page' }
})
export class MemberSearchComponent implements AfterViewInit {
  @Input() stateMessage!: string;
  @Output() onCancel = new EventEmitter<null>();
  @Output() onPick = new EventEmitter<Member>();

  private memberService = inject(MemberService);
  private errorServ = inject(ErrorNotifierService);

  @ViewChild(IonSearchbar) searchBar!: IonSearchbar;
  members$!: Observable<Member[]>;

  ngAfterViewInit() {
    setTimeout(() => { this.searchBar.setFocus(); }, 50);
  }

  cancel() {
    console.log('[Component: MemberSearch] User canceled search');
    this.onCancel.emit();
  }

  returnMember(member: Member) {
    console.log(`[Component: MemberSearch] Picked member: '${member.display_name}'`);
    this.onPick.emit(member);
  }

  onSearchChange(event: Event) {
    const query = (event.target as HTMLInputElement).value;
    this.members$ = this.memberService.searchMember(query).pipe(
      catchError((err) => {
        console.log(`[Component: MemberSearch] MemberService returned error on search: ${err}`);
        this.errorServ.notifyError(err);
        this.cancel();
        return of([]);  // Show empty list in case of error
      })
    );
  }

}
