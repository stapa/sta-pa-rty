import { TestBed, fakeAsync, flush, flushMicrotasks, tick } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController, TestRequest } from '@angular/common/http/testing'
import { AuthenticationService } from './authentication.service';
import { Browser } from '__mocks__/@capacitor/browser';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { SecureStoragePlugin } from 'capacitor-secure-storage-plugin';
import { Router } from '@angular/router';
import { effect } from '@angular/core';


describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let httpTest: HttpTestingController;
  let router: Router;
  // const isNative = Capacitor.isNativePlatform();

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });

    await SecureStoragePlugin.clear();
    service = TestBed.inject(AuthenticationService);
    httpTest = TestBed.inject(HttpTestingController);
    router = TestBed.inject(Router);

    Browser.testReset();
    spyOn(Browser, 'open');
    spyOn(router, 'navigateByUrl');
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('does not have open browsers at startup', () => {
    expect(Browser.opened).toBeFalse();
  });

  it('sets loggedIn$ to false', (done) => {
    TestBed.runInInjectionContext(() => {
      effect(() => {
        if(service.loggedIn() === false) {
          done();
        }
      })
    });
  });

  describe('startLogin', () => {
    const loginUrl = "https://127.0.0.1/test/loginUrl";
    const backendUrl = environment.baseUrl + '/auth/login'
    let startLogin$: Observable<undefined>;

    const respondAlreadyLoggedIn = () => {
      const authReq = httpTest.expectOne(backendUrl);
      authReq.flush({ description: "Already logged in." }, { status: 400, statusText: "" });
    };

    const respondLoginUrl = () => {
      const authReq = httpTest.expectOne(backendUrl);
      authReq.flush({ login_url: loginUrl });
    };

    const respondServerError = () => {
      const authReq = httpTest.expectOne(backendUrl);
      authReq.flush({}, { status: 500, statusText: "" });
    };
      
    beforeEach(() => {
      startLogin$ = service.startLogin();
    });

    it('makes POST call to /auth/login', () => {
      startLogin$.subscribe();
      const authReq = httpTest.expectOne(backendUrl);
      expect(authReq.request.method).toBe('POST');
    });

    it('opens in-app browser with loginUrl', () => {
      startLogin$.subscribe();
      respondLoginUrl();
      expect(Browser.open).toHaveBeenCalledOnceWith({ url: loginUrl, windowName: '_self'});
    });

    it('never emits or completes if no error', fakeAsync(() => {
      let emitsOrCompletes = false;
      startLogin$.subscribe({
        next: () => { emitsOrCompletes = true; },
        complete: () => { emitsOrCompletes = true; }
      });
      respondLoginUrl();
      tick(500);
      expect(emitsOrCompletes).toBeFalse();
    }));

    it('emits and completes when in-app browser is closed on native platforms', (done) => {
      let emits = false;
      startLogin$.subscribe({
        next: () => { emits = true; },
        complete: () => { done(); }
      });
      respondLoginUrl();
      Browser.testClose();
      expect(emits).toBeTrue();
    });

    describe('if session already exists', () => {
      it('does not open in-app browser', () => {
        startLogin$.subscribe();
        respondAlreadyLoggedIn();
        expect(Browser.open).not.toHaveBeenCalled();
      });

      it('emits and completes', (done) => {
        let emits = false;
        startLogin$.subscribe({
          next: () => { emits = true; },
          complete: () => { done(); }
        });
        respondAlreadyLoggedIn();
        expect(emits).toBeTrue();
      });

      it('redirects to /', () => {
        startLogin$.subscribe();
        respondAlreadyLoggedIn();
        expect(router.navigateByUrl).toHaveBeenCalledOnceWith('/');
      });

      it('causes loggedIn$ to emit true', () => {
        startLogin$.subscribe();
        respondAlreadyLoggedIn();
        expect(service.loggedIn()).toBeTrue();
      });
    });

    // unexpected backend errors are handled with interceptors globally, they
    // are not a responsability of this service
    it('throws error if backend return error', (done) => {
      startLogin$.subscribe({ error: () => { done(); }});
      respondServerError();
    });
  });

  describe('handleCallback', () => {
    
    const backendUrl = environment.baseUrl + '/me'

    const respondAuthenticated = () => {
      const httpReq = httpTest.expectOne(backendUrl);
      httpReq.flush({ username: "testuser@test.com" });
    };

    const respondNotAuthenticated = () => {
      const httpReq = httpTest.expectOne(backendUrl);
      httpReq.flush({}, { status: 401, statusText: "" });
    };

    const respondServerError = () => {
      const httpReq = httpTest.expectOne(backendUrl);
      httpReq.flush({}, { status: 500, statusText: "" });
    }

    it('makes GET call to /me', () => {
      service.handleCallback();
      const httpReq = httpTest.expectOne(backendUrl);
      expect(httpReq.request.method).toBe('GET');
    });

    describe('if authenticated', () => {
      it('causes loggedIn$ to emit true', () => {
        service.handleCallback();
        respondAuthenticated();
        expect(service.loggedIn()).toBeTrue();
      });
  
      it('redirects to /', () => {
        service.handleCallback();
        respondAuthenticated();
        expect(router.navigateByUrl).toHaveBeenCalledOnceWith('/');
      });
    });

    describe('if not authenticated', () => {
      it('causes loggedIn$ to emit false', () => {
        service.handleCallback();
        respondNotAuthenticated();
        expect(service.loggedIn()).toBeFalse();
      });
  
      it('redirects to /login', () => {
        service.handleCallback();
        respondNotAuthenticated();
        expect(router.navigateByUrl).toHaveBeenCalledOnceWith('/login');
      });
    });

    it('throws error if backend return error', fakeAsync(() => {
        service.handleCallback();
        respondServerError();
        expect(() => { flush(); }).toThrow(); 
    }));
  });

  describe('startLogout', () => {
    const logoutUrl = "https://127.0.0.1/test/logoutUrl";
    const backendUrl = environment.baseUrl + '/auth/logout';

    const respondLogoutUrl = () => {
      const httpReq = httpTest.expectOne(backendUrl);
      httpReq.flush({ logout_url: logoutUrl });
    };

    const respondServerError = () => {
      const httpReq = httpTest.expectOne(backendUrl);
      httpReq.flush({}, { status: 500, statusText: "" });
    }

    it('makes POST call to /auth/logout', () => {
      service.startLogout().subscribe();
      const httpReq = httpTest.expectOne(backendUrl);
      expect(httpReq.request.method).toBe('POST');
    });

    it('opens in-app browser with logoutUrl', () => {
      service.startLogout().subscribe();
      respondLogoutUrl();
      expect(Browser.open).toHaveBeenCalledOnceWith({ url: logoutUrl, windowName: '_self'});
    });

    it('sets loggedIn$ to false', (done) => {
      TestBed.runInInjectionContext(() => {
        effect(() => {
          if(service.loggedIn() === false) {
            done();
          }
        })
      });
      service.startLogout().subscribe();
      respondLogoutUrl();
    });

    it('throws error if backend return error', (done) => {
      service.startLogout().subscribe({ error: (error) => {
        expect(error).toBeTruthy();
        done();
      }});
      respondServerError();
    });


  });

  afterEach(() => {
    httpTest.verify();
  });
});
