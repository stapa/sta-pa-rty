import { Injectable, NgZone, inject, signal } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable, ReplaySubject, Subject, catchError, filter, fromEvent, fromEventPattern, map, of, switchMap, take, tap } from 'rxjs';
import { Capacitor } from '@capacitor/core';
import { Browser, OpenOptions } from '@capacitor/browser';
import { SecureStoragePlugin } from 'capacitor-secure-storage-plugin';
import { environment } from '../../../environments/environment';
import { ApiService } from '../api/api.service';

const isNative = Capacitor.isNativePlatform();

interface LoginData {
  login_url: string
}

interface LogoutData {
  logout_url: string
}

interface UserInfo {
  username: string
}

export interface LoginConf {
  qa_enabled: boolean;
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private api = inject(ApiService);
  private router = inject(Router);
  private zone = inject(NgZone);
  loggedIn = signal<boolean|undefined>(undefined);
  readonly loginConf$ = this.api.get<LoginConf>('login_conf');
  
  constructor() { 
    console.log('[Service: Authentication] Checking stored loggedIn state');
    this.storeLoggedInGet().then((loggedIn) => {
      console.log(`[Service: Authentication] Stored logged in status is: ${loggedIn}`);
      this.loggedIn.set(loggedIn);
    });
  }

  private async storeLoggedInSet() {
    await SecureStoragePlugin.set({ key: "loggedIn", value: "DUMMY" });
  }

  private async storeLoggedInGet() {
    try {
      await SecureStoragePlugin.get({ key: "loggedIn" });
      return true;
    } catch (e) {
      return false;
    }
  }

  private async storeLoggedInUnset() {
    try {
      await SecureStoragePlugin.remove({ key: "loggedIn" });
    } catch(e) {
      return;
    }
  }

  private openBrowser(openOptions: OpenOptions) {
    const browserClosed$ = fromEventPattern<undefined>(
      (handler) => {
        return Browser.addListener('browserFinished', () => {
          this.zone.run(() => { handler(); });
        });
      },
      (_, signal) => { signal.remove(); }
    );
    Browser.open({
      windowName: isNative ? '_blank' : '_self',
      ...openOptions
    });
    return browserClosed$.pipe(
      tap(() => { console.log("[Service: Authentication] In-app browser closed")}),
      take(1)
    );
  }

  private onLoginSuccess() {
    console.log("[Service: Authentication] Login successful, navigating to /");
    this.storeLoggedInSet();
    this.loggedIn.set(true);
    this.router.navigateByUrl('/');
  }

  private catchAlreadyLoggedIn(error: HttpErrorResponse) {
    const subj = new Subject<void>();
    subj.asObservable()

    if (error.status == 400 && error.error.description == "Already logged in.") {
      console.log('[Service: Authentication] Valid backend session already exists');
      this.onLoginSuccess();
      return of(undefined);
    } else {
      throw error;
    }
  }

  private catchFailedLogIn(error: HttpErrorResponse) {
    if (error.status == 401) {
      console.log('[Service: Authentication] Login failed: valid backend session does not exist');
      this.handleLogout();
      return of();
    } else {
      throw error;
    }
  }

  private verifyLoginResult() {
    console.log("[Service: Authentication] Verifying if valid backend session exists")
    this.api.get<UserInfo>('me')
      .subscribe({
        next: (userInfo) => { this.onLoginSuccess(); },
        error: (e) => { this.catchFailedLogIn(e); }
      });
  }

  private getLoginUrl() {
    const callbackUrl =
      (isNative ? environment.appUrl : window.origin) + '/loginCallback';
    console.log(`[Service: Authentication] Using callback url: ${callbackUrl}`);
    return this.api.post<LoginData>(
      'auth/login',
      { redirect_url: callbackUrl, native_app: isNative },
    ).pipe(
      map((loginData) => (loginData.login_url)),
      tap((loginUrl) => {
        console.log("[Service: Authentication] Login redirect url from backend: " + loginUrl);
      })
    );
  }

  startLogin(): Observable<undefined> {
    return this.getLoginUrl().pipe(
      tap(() => { console.log("[Service: Authentication] Starting in-app browser"); }),
      switchMap((loginUrl) => { return this.openBrowser({ url: loginUrl }); }),
      // 'browserFinished' emits prematurerly if platform is not native, in
      // that case we can never emits and keep subscriptions open forever, as
      // the whole App will be destroyed on the redirect towards the login page
      //filter(() => isNative),
      catchError((e: HttpErrorResponse) => { return this.catchAlreadyLoggedIn(e); }),
    );
  }

  startLogout() {
    console.log("[Service: Authentication] Logging out from backend.")
    return this.api.post<LogoutData>('auth/logout', {})
      .pipe(
        tap(() => {
          console.log("[Service: Authentication] Logged out from backend, opening browser for Azure logout.");
          this.handleLogout();
        }),
        switchMap((logoutData) => { return this.openBrowser({ url: logoutData.logout_url})})
      );
  }      

  handleCallback() {
    console.log("[Service: Authentication] Handling login callback");

    // This is required only for iOS, otherwise the login In-App Browser remains open.
    // On Android the In-App Browser is automatically closed when clicking the deeplink.
    // On Browsers there is not In-App Browser, its just a redirect in the same tab.
    if (Capacitor.getPlatform() == 'ios') {
      Browser.close();
    }

    this.verifyLoginResult();
    // Now if cookie is valid same a dummy token
    // TODO: get token from callback and save it
  }

  handleLogout() {
    this.storeLoggedInUnset();
    this.loggedIn.set(false);
    this.router.navigateByUrl('/login');
  }
}
