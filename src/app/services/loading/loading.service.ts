import { Injectable, computed, effect, inject, signal } from '@angular/core';
import { LoadingController } from '@ionic/angular/standalone';
import { Observable, finalize } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {
  private loadingCtrl = inject(LoadingController);
  readonly loadingCount = signal(0);
  readonly loadingMessage = signal('');
  readonly isLoading = computed(() => this.loadingCount() > 0);
  private loadingElement: HTMLIonLoadingElement|undefined;

  constructor() {
    effect(async () => {
      if (this.isLoading()) {
        this.loadingElement?.dismiss();
        this.loadingElement = await this.loadingCtrl.create({message: this.loadingMessage()});
        await this.loadingElement.present();
      } else {
        await this.loadingElement?.dismiss();
      }
    });
  }

  setMessage(message: string) {
    this.loadingMessage.set(message);
  }

  trackLoading<T>(message?: string) {
    return (source: Observable<T>): Observable<T> => {
      this.loadingMessage.set(message ? message : '');
      this.loadingCount.update((count) => count + 1);
      return source.pipe(
        finalize(() => this.loadingCount.update((count) => count - 1))
      );
    };
  }
}
