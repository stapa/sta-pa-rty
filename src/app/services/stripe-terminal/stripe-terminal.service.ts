import { Injectable, effect, inject, signal } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { DiscoveryMethod, PaymentIntent, StripeTerminalPlugin } from 'zolfa-capacitor-stripe-terminal';
import {
  Observable,
  concatMap,
  finalize,
  firstValueFrom,
  last,
  map,
  repeat,
  takeWhile,
  tap,
} from 'rxjs';
import { Span, trace, context, SpanStatusCode } from '@opentelemetry/api';

import { environment } from 'src/environments/environment';
import { ApiService } from '../api/api.service';
import { AuthenticationService } from '../authentication/authentication.service';
import { LoadingService } from '../loading/loading.service';
import { TelemetryService } from '../telemetry/telemetry.service';
import { ErrorNotifierService } from '../error-notifier/error-notifier.service';

export interface Payment {
  member_id: number,
  amount: number, // cents of euro
  reason: 'topup' | 'membership',
}

export interface PaymentIntentRemote {
  id: string,
  client_secret: string
}

type TerminalState =
  'disconnected'
  | 'connecting'
  | 'standby'
  | 'processing'
  | 'waiting_for_card'
  | 'waiting_for_capture'

@Injectable({
  providedIn: 'root'
})
export class StripeTerminalService {
  state = signal<TerminalState>('disconnected');
  missingCap = signal<'noLocation'|null>(null);

  private terminal!: StripeTerminalPlugin;
  private api = inject(ApiService);
  private auth = inject(AuthenticationService);
  private loadService = inject(LoadingService);
  private telemetryService = inject(TelemetryService);
  private connectParentSpan!: Span;
  private errorNotifier = inject(ErrorNotifierService);

  private missingCapEffect = effect(() => {
    switch(this.missingCap()) {
      case 'noLocation':
        this.errorNotifier.notifyError(
          'Enable Location on your phone to accept card payments, then restart the app.'
          );
        break;
    }
  });
  private logOutEffect = effect(() => {
    if (this.auth.loggedIn() === false) {
      this.clear();
    }
  });

  private stateChangeEffect = effect(() => {
    console.log('[Service: StripeTerminal] State changed to: ' + this.state().toUpperCase());
  });

  async clear() {
    if (this.state() != 'disconnected' && this.state() != 'connecting') {
      console.log(`[Service: StripeTerminal] Disconnecting, current status: ${this.state()}`);
      if(await this.terminal.getConnectedReader()) {
        console.log('[Service: StripeTerminal] Connected readers found, waiting for disconnection');
        await this.terminal.disconnectReader();
      }
      this.state.set('disconnected');
      console.log('[Service: StripeTerminal] All readers disconnected');
    }

    if (this.terminal?.isInitialized) {
      console.log('[Service: StripeTerminal] Clearing access token.');
      await this.terminal.clearCachedCredentials();
    } else {
      console.log('[Service: StripeTerminal] Not initialized.')
    }

  }

  async connect() {
    // wait for tracer to available before connecting
    const tracer = await firstValueFrom(this.telemetryService.getTracer());
    return tracer.startActiveSpan('connectTerminal', async (span: Span) => {
        this.connectParentSpan = span;
        try {
          return await this._connect();
        } catch(error: any) {
          span.recordException(error);
          span.setStatus({ code: SpanStatusCode.ERROR });
          throw error;
        } finally {
          span.end();
        }
    });
  }

  async _connect() {
    if(this.state() != 'disconnected') {
      return true;
    }

    if (Capacitor.getPlatform() !== 'android') {
      throw "[Service: StripeTerminal] Only android is supported at the moment"
    }
    this.state.set('connecting');
    this.missingCap.set(null);
    try {
      let response = await StripeTerminalPlugin.checkPermissions();
      if (response.location === 'prompt') {
        response = await StripeTerminalPlugin.requestPermissions();
      }
      if (response.location !== 'granted') {
        window.alert('Position permission required! Open Android app settings and manually enable it. Then restart the app.');
        throw '[Service: StripeTerminal] No location permission, impossible to connect.';
      }
      if (!this.terminal?.isInitialized) {
        this.terminal = await StripeTerminalPlugin.create({
          fetchConnectionToken: () => (this.fetchConnectionToken()),
          onUnexpectedReaderDisconnect: () => { this.onUnexpectedReaderDisconnect() }
        });
        console.log('[Service: StripeTerminal] Connect: SDK initialized.');
      } else {
        console.log('[Service: StripeTerminal] Connect: SDK already initialized.');
      }
      if (!await this.terminal.getConnectedReader()) {
        const readers = await firstValueFrom(this.terminal.discoverReaders({
          simulated: !environment.production,
          discoveryMethod: DiscoveryMethod.LocalMobile,
        }));
        if (readers.length == 0) {
          throw "[Service: StripeTerminal] Connect: Discovery failed: no terminal found.";
        }
        console.log('[Service: StripeTerminal] Connect: Terminal discovered, connecting');
        await this.terminal.connectLocalMobileReader(readers[0], { locationId: environment.stripeLocationId });
      } else {
        console.log('[Service: StripeTerminal]: Connect: Already connected.');
      }
      this.state.set('standby');
    } catch (error: any) {
      console.log(`[Service: StripeTerminal] Connecion failed. ${error}`);
      this.state.set('disconnected');
      if(error?.message === "Location services must be enabled to use Terminal") {
        this.missingCap.set('noLocation');
      }
      throw error;
    }
    return true;
  }

  private onUnexpectedReaderDisconnect() {
    console.log('[Service: StripeTerminal] Connection lost');
    this.state.set('disconnected');
  }

  private async fetchConnectionToken() {
    const tracer = await firstValueFrom(this.telemetryService.getTracer());
    const ctx = trace.setSpan(context.active(), this.connectParentSpan);
    const fnSpan = tracer.startSpan('fetchToken', {}, ctx);
    const request = this.api.post('terminal/stripe/token', {}).pipe(
      map((response: any): string => response.secret),
      tap((token) => {
        console.log(`[Service: StripeTerminal] Token fetched: ***${token.slice(-5)}`);
      }),
      finalize(() => { fnSpan.end(); })
    );
    return await context.with(trace.setSpan(context.active(), fnSpan), async () => {
      return await firstValueFrom(request);
    });
  }

  startPayment(payment: Payment): Observable<string> {
    if (this.state() == 'standby') {
      this.state.set('processing')
    } else {
      throw '[Service: StripeTerminal] Terminal is not ready. Current state: ' + this.state();
    }
    return this.api.post<PaymentIntentRemote>('terminal/stripe/payment', payment)
      .pipe(
        concatMap((pi) => this.terminal.retrievePaymentIntent(pi.client_secret)),
        tap(() => {
          this.state.set("waiting_for_card");
          this.loadService.setMessage('Waiting for card...');
        }),
        concatMap((res) => this.terminal.collectPaymentMethod()),
        tap(() => {
          this.state.set("processing");
          this.loadService.setMessage('Contacting issuer...');
        }),
        concatMap(() => this.terminal.processPayment()),
        tap(() => {
          this.state.set("waiting_for_capture");
          this.loadService.setMessage('Verifying capture...');
        }),
        concatMap((paymentIntent) => this.waitForCapture(paymentIntent)),
        finalize(() => this.state.set("standby")),
        this.loadService.trackLoading('Preparing POS payment...'),
      );
  }

  private waitForCapture(paymentIntent: PaymentIntent | null) {
    if (!paymentIntent) {
      throw new Error('Unexpected null PaymentIntent');
    }
    const url = 'terminal/stripe/payment/' + encodeURIComponent(paymentIntent.stripeId);
    
    return this.api.get<any>(url).pipe(
      map((paymentIntent): string => (paymentIntent.status)),
      tap((status) => console.log('[Service: StripeTerminal] Current payment status: ' + status)),
      repeat({count: 10, delay: 1000}),
      takeWhile((status) => status === 'processing', true),
      last(),
      tap((status) => {
        if (status == 'processing') {
          throw new Error('Payment verification timed out');
        } else if (status !== 'succeeded') {
          throw new Error('Payment verification failed');
      }})
    );
  }

}
