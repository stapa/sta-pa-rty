import { TestBed, fakeAsync, flush, flushMicrotasks, tick} from '@angular/core/testing';
import { StripeTerminalService, Payment } from './stripe-terminal.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { Capacitor } from '@capacitor/core';
import { StripeTerminalPlugin } from 'zolfa-capacitor-stripe-terminal';
import { environment } from 'src/environments/environment';
import { Subscription, of } from 'rxjs';
import { TelemetryService } from '../telemetry/telemetry.service';
import { trace, Tracer } from '@opentelemetry/api';
import { signal } from '@angular/core';
import { AuthenticationService } from '../authentication/authentication.service';

class TelemetryStub {
  //getTracer() {
  //  return new Observable((subscriber) => {
  //    subscriber.next(trace.getTracer('fakeTracer'));
  //  }).pipe(take(1));
  //}
  getTracer() {
    return of(trace.getTracer('fakeTracer'));
  }
}

class AuthenticationStub {
  loggedIn = signal<boolean|undefined>(true);
}

describe('StripeTerminalService', () => {
  let service: StripeTerminalService;
  let httpTest: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: TelemetryService, useClass: TelemetryStub },
        { provide: AuthenticationService, useClass: AuthenticationStub }
      ]
    });
    httpTest = TestBed.inject(HttpTestingController);
    service = TestBed.inject(StripeTerminalService);

    // @ts-ignore
    StripeTerminalPlugin._holdCompleteConnection$.next(false);
  });

  it('should be created', () => {
     expect(service).toBeTruthy();
  });

  it('has disconnected state at start', () => {
     expect(service.state()).toBe('disconnected');
  });

  it('on web platform connect() is rejected', async () => {
     await expectAsync(service.connect()).toBeRejected();
     expect(service.state()).toBe('disconnected');
  });

  describe('on android platform', () => {
    beforeEach(() => {
      spyOn(Capacitor, 'getPlatform').and.returnValue('android');
      spyOn(Capacitor, 'isNativePlatform').and.returnValue(true);
      StripeTerminalPlugin.prototype.isInitialized = false;
    });

    it('connect() resolves and changes states', fakeAsync(() => {
      // @ts-ignore
      StripeTerminalPlugin._holdCompleteConnection$.next(true);
      let finished = false;
      service.connect().then(() => { finished = true; });
      flushMicrotasks();
      expect(service.state()).toBe('connecting');
      // @ts-ignore
      StripeTerminalPlugin._holdCompleteConnection$.next(false);
      flush();
      expect(finished).toBeTrue();
      expect(service.state()).toBe('standby');
    }));

    it('connect() reverts state to disconnected on error', async () => {
      spyOn(StripeTerminalPlugin.prototype, 'connectLocalMobileReader').and.rejectWith(new Error('TestError'));      
      await expectAsync(service.connect()).toBeRejectedWithError('TestError');
      expect(service.state()).toBe('disconnected');
    });

    describe('startPayment()', () => {
      const payment: Payment = { member_id: 1, amount: 10000, reason: 'topup'};
      let paymentSub: Subscription;
      let paymentSubLastError: any;
      let paymentSubLast: string|undefined;

      beforeEach(async () => {
        await service.connect();
        paymentSubLast = undefined;
        paymentSubLastError = undefined;
        paymentSub = service.startPayment(payment).subscribe({
          next: (result) => { paymentSubLast = result; },
          error: (error) => { paymentSubLastError = error; }
        });
      });

      it('calls backend to generate PaymentIntent', () => {
        const reqPay = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment');
        expect(reqPay.request.method).toBe('POST');
        expect(reqPay.request.body).toBe(payment);
      });

      it('calls plugin to retrieve PaymentIntent', fakeAsync(() => {
        spyOn(StripeTerminalPlugin.prototype, 'retrievePaymentIntent').and.rejectWith(new Error('TestError'));
        const reqPay = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment');
        reqPay.flush({ client_secret: 'pi_testPI_secret_testClientSecret'});
        flush();
        expect(StripeTerminalPlugin.prototype.retrievePaymentIntent).toHaveBeenCalledOnceWith("pi_testPI_secret_testClientSecret");
      }));

      it('calls plugin to collect payment methods', fakeAsync(() => {
        spyOn(StripeTerminalPlugin.prototype, 'collectPaymentMethod').and.rejectWith(new Error('TestError'));
        const reqPay = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment');
        reqPay.flush({ client_secret: 'pi_testPI_secret_testClientSecret'});
        flush();
        expect(StripeTerminalPlugin.prototype.collectPaymentMethod).toHaveBeenCalledOnceWith();
      }));
      
      it('calls plugin to process payment', fakeAsync(() => {
        spyOn(StripeTerminalPlugin.prototype, 'processPayment').and.rejectWith(new Error('TestError'));
        const reqPay = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment');
        reqPay.flush({ client_secret: 'pi_testPI_secret_testClientSecret'});
        flush();
        expect(StripeTerminalPlugin.prototype.processPayment).toHaveBeenCalledOnceWith();
      }));

      it('calls backend to verify payment', fakeAsync(() => {
        const reqPay = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment');
        reqPay.flush({ client_secret: 'pi_testPI_secret_testClientSecret'});
        flush();
        const reqVerify = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment/pi_testPI');
        expect(reqVerify.request.method).toBe('GET');
      }));

      it('emits error if call to backend to generate payment fails', fakeAsync(() => {
        const reqPay = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment');
        reqPay.error(new ProgressEvent('network error'));
        flush();
        expect(paymentSubLast).toBeUndefined();
        expect(paymentSubLastError.error).toEqual(new ProgressEvent('network error'));
      }));

      it('emits error if call to plugin collectPaymentMethod() fails', fakeAsync(() => {
        spyOn(StripeTerminalPlugin.prototype, 'collectPaymentMethod').and.rejectWith(new Error('TestError'));
        const reqPay = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment');
        reqPay.flush({ client_secret: 'pi_testPI_secret_testClientSecret'});
        flush();
        expect(paymentSubLast).toBeUndefined();
        expect(paymentSubLastError).toEqual(new Error('TestError'));
      }));

      it('emits error if call to plugin processPayment() fails', fakeAsync(() => {
        spyOn(StripeTerminalPlugin.prototype, 'collectPaymentMethod').and.rejectWith(new Error('TestError'));
        const reqPay = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment');
        reqPay.flush({ client_secret: 'pi_testPI_secret_testClientSecret'});
        flush();
        expect(paymentSubLast).toBeUndefined();
        expect(paymentSubLastError).toEqual(new Error('TestError'));
      }));
  
      it('emits error if call to backend to verify payment fails', fakeAsync(() => {
        const reqPay = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment');
        reqPay.flush({ client_secret: 'pi_testPI_secret_testClientSecret'});
        flush();
        const reqVerify = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment/pi_testPI');
        reqVerify.error(new ProgressEvent('network error'));
        flush();
        expect(paymentSubLast).toBeUndefined();
        expect(paymentSubLastError.error).toEqual(new ProgressEvent('network error'));
      }));

      it('emits error if backend return unsuccesful verification', fakeAsync(() => {
        const reqPay = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment');
        reqPay.flush({ client_secret: 'pi_testPI_secret_testClientSecret'});
        flush();
        const reqVerify = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment/pi_testPI');
        reqVerify.flush({ status: 'failed' });
        flush();
        expect(paymentSubLast).toBeUndefined();
        expect(paymentSubLastError).toEqual(new Error('Payment verification failed'));
      }));

      it('repeat call to backend if verification status is processing and timeout eventually', fakeAsync(() => {
        const reqPay = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment');
        reqPay.flush({ client_secret: 'pi_testPI_secret_testClientSecret'});
        flush();
        for (let i = 0; i < 9; i++) {
          const reqVerify = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment/pi_testPI');
          reqVerify.flush({ status: 'processing' });
          flush();
          expect(paymentSubLast).toBeUndefined();
          expect(paymentSubLastError).toBeUndefined();
          tick(1000); 
        }
        const reqVerify = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment/pi_testPI');
        reqVerify.flush({ status: 'processing' });
        flush();
        expect(paymentSubLast).toBeUndefined();
        expect(paymentSubLastError).toEqual(new Error('Payment verification timed out'));
      }));

      it('emits without error if backend payment verification successful', fakeAsync(() => {
        const reqPay = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment');
        reqPay.flush({ client_secret: 'pi_testPI_secret_testClientSecret'});
        flush();
        const reqVerify = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment/pi_testPI');
        reqVerify.flush({ status: 'succeeded' });
        flush();
        expect(paymentSubLast).toBeDefined();
        expect(paymentSubLastError).toBeUndefined();
      }));

      it('emits without error if backend payment verification successful after progressing', fakeAsync(() => {
        const reqPay = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment');
        reqPay.flush({ client_secret: 'pi_testPI_secret_testClientSecret'});
        flush();
        const reqVerifyProg = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment/pi_testPI');
        reqVerifyProg.flush({ status: 'processing' });
        flush();
        expect(paymentSubLast).toBeUndefined();
        expect(paymentSubLastError).toBeUndefined();
        tick(1000); 
        const reqVerify = httpTest.expectOne(environment.baseUrl + '/terminal/stripe/payment/pi_testPI');
        reqVerify.flush({ status: 'succeeded' });
        flush();
        expect(paymentSubLast).toBeDefined();
        expect(paymentSubLastError).toBeUndefined();
      }));

      afterEach(() => {
        paymentSub.unsubscribe();
      });
    });
  });

  afterEach(() => {
    httpTest.verify();
    console.log('finished');
  })




});
