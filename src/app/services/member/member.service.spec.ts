import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing'
import { MemberService, Member } from './member.service';
import { environment } from 'src/environments/environment';

describe('MemberService', () => {
  let service: MemberService;
  let httpTest: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({imports: [HttpClientTestingModule]});
    service = TestBed.inject(MemberService);
    httpTest = TestBed.inject(HttpTestingController);
    
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('searchMember', () => {
    const query = "testQuerySearchString";
    const backendUrl = environment.baseUrl + '/members';
    
    it('makes GET call to /members', () => {
      service.searchMember(query).subscribe();
      const httpReq = httpTest.expectOne(backendUrl + '?search=' + query);
      expect(httpReq.request.method).toBe("GET");
      expect(httpReq.request.params.get('search')).toBe(query);
    });

    it('return results provided by the backend', (done) => {
      const members: Member[] = [
        { id: 1, display_name: "member1", balance: 0 },
        { id: 2, display_name: "member2", balance: 0 }
      ];
      service.searchMember(query).subscribe((result) => {
        expect(result).toBe(members);
        done();
      });
      const httpReq = httpTest.expectOne(backendUrl + '?search=' + query);
      httpReq.flush(members);
    });
  });

  describe('fromQrcode', () => {
    const qrcode = "666666";
    const backendUrl = environment.baseUrl + '/members';
    
    it('makes GET call to /members', () => {
      service.fromQrcode(qrcode).subscribe();
      const httpReq = httpTest.expectOne(backendUrl + '?qrcode=' + qrcode);
      expect(httpReq.request.method).toBe("GET");
      expect(httpReq.request.params.get('qrcode')).toBe(qrcode);
    });

    it('if at least one member is available, returns the first', (done) => {
      const members: Member[] = [
        { id: 1, display_name: "member1", balance: 0 },
        { id: 2, display_name: "member2", balance: 0 }
      ];
      service.fromQrcode(qrcode).subscribe((result) => {
        expect(result).toBe(members[0]);
        done();
      });
      const httpReq = httpTest.expectOne(backendUrl + '?qrcode=' + qrcode);
      httpReq.flush(members);
    });

    it('if no member is available, returns null', (done) => {
      const members: Member[] = [];
      service.fromQrcode(qrcode).subscribe((result) => {
        expect(result).toBeNull();
        done();
      });
      const httpReq = httpTest.expectOne(backendUrl + '?qrcode=' + qrcode);
      httpReq.flush(members);
    });
  });

  afterEach(() => {
    httpTest.verify();
  });
});
