import { Injectable, inject } from '@angular/core';
import { map } from 'rxjs';
import { ApiService } from '../api/api.service';


export interface Member {
  id: number,
  display_name: string,
  pronouns?: string
  balance: number
}

@Injectable({
  providedIn: 'root'
})
export class MemberService {
  private api = inject(ApiService);

  fromQrcode(qrcode: string) {
    return this.api.get<Member[]>('members', { qrcode: qrcode })
      .pipe(
        map((members) => (members.length == 0 ? null : members[0]))
      );
  }

  searchMember(query: string) {
    return this.api.get<Member[]>('members', { search: query });  
  }

}
