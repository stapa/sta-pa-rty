import { Injectable, inject } from '@angular/core';
import { App } from '@capacitor/app';
import { Device } from '@capacitor/device';
import { Capacitor } from '@capacitor/core';
import opentelemetry, { Tracer } from '@opentelemetry/api';
import { IResource, Resource } from '@opentelemetry/resources';
import { BatchSpanProcessor, WebTracerProvider } from '@opentelemetry/sdk-trace-web';
import { SemanticResourceAttributes } from '@opentelemetry/semantic-conventions';
import { OTLPTraceExporter } from '@opentelemetry/exporter-trace-otlp-http';
import { ReplaySubject, firstValueFrom } from 'rxjs';
import { ZoneContextManager } from '@opentelemetry/context-zone-peer-dep';
import { B3InjectEncoding, B3Propagator } from '@opentelemetry/propagator-b3';
import { registerInstrumentations } from '@opentelemetry/instrumentation';
import { getWebAutoInstrumentations } from '@opentelemetry/auto-instrumentations-web';

import { environment } from 'src/environments/environment';
import { ApiService } from '../api/api.service';
import { StripeTerminalPluginInstrumentation } from 'src/app/utils/instrument-stripe-terminal';


interface TelemetryConfiguration {
  otlp_url: string,
  otlp_headers: {[key: number]: string},
  otlp_b3_whitelist: [string]
}

@Injectable({
  providedIn: 'root'
})
export class TelemetryService {
  private api = inject(ApiService);
  private tracerProvider!: WebTracerProvider;
  private conf!: TelemetryConfiguration; 
  private contextManager = new ZoneContextManager();
  private tracer = new ReplaySubject<Tracer>(1);

  getTracer() {
    return this.tracer.asObservable();
  }

  async start() {
    const resource = await this.enrichNativeResources(this.initResources());
    this.conf = await firstValueFrom(
      this.api.get<TelemetryConfiguration>('telemetry')
    );

    this.tracerProvider = new WebTracerProvider({ resource: resource });
    this.tracerProvider.addSpanProcessor(
      new BatchSpanProcessor(
        new OTLPTraceExporter({
          url: this.conf.otlp_url + '/v1/traces',
          headers: this.conf.otlp_headers,
        })
      )
    );
    this.tracerProvider.register({
      contextManager: this.contextManager,
      propagator: new B3Propagator({ injectEncoding: B3InjectEncoding.MULTI_HEADER }),
    });

    registerInstrumentations({
      instrumentations: [
        new StripeTerminalPluginInstrumentation(),
        getWebAutoInstrumentations({
          '@opentelemetry/instrumentation-xml-http-request': {
            propagateTraceHeaderCorsUrls: this.conf.otlp_b3_whitelist.map((regex) => new RegExp(regex))
          }
        })
      ]
    });
    this.tracer.next(opentelemetry.trace.getTracer('sta-pa-rty'));
  }

  private initResources(): IResource {
    return Resource
      .default()
      .merge(new Resource({
        [SemanticResourceAttributes.SERVICE_NAME]: 'frontend-staparty',
        [SemanticResourceAttributes.SERVICE_VERSION]: environment.appVersion,
        [SemanticResourceAttributes.DEPLOYMENT_ENVIRONMENT]: environment.deployment,
        ['capacitor.platform']: Capacitor.getPlatform()
      }));
  }

  private async enrichNativeResources(resource: IResource) {
    if (!Capacitor.isNativePlatform()) {
      return resource;
    }
    const appInfo = await App.getInfo();
    const deviceInfo = await Device.getInfo();
    return resource.merge(new Resource({
      ['capacitor.build']: appInfo.build,
      [SemanticResourceAttributes.DEVICE_MODEL_IDENTIFIER]: deviceInfo.model,
      [SemanticResourceAttributes.OS_VERSION]: deviceInfo.osVersion,
    }));
  }


  constructor() { }
}
