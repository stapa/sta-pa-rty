import { Injectable, inject } from '@angular/core';
import { Member } from '../member/member.service';
import { Observable, delay, of } from 'rxjs';
import { ApiService } from '../api/api.service';

export interface TransactionReq {
  member_id: number,
  cash_in: number,
  lines: TransactionReqLine[]
}

export interface TransactionReqLine {
  type: 'beer',
  quantity: number
}

export interface TransactionResult {
  executed: boolean,
  balance_before: number,
  balance_after: number,
  cash_in?: number,
  lines: TransactionResLine[]
}

export interface TransactionResLine {
  type: 'beer'|'free_beer',
  quantity: number,
  unit_price: number
}

export interface TransactionStatistics { 
  number_free: number,
  number_paid: number,
  earnings: number
}


@Injectable({
  providedIn: 'root'
})
export class TransactionService {
  private api = inject(ApiService);

  prepareTxReq(member: Member, beers: number, cashIn?: number): TransactionReq {
    return {
      member_id: member.id,
      cash_in: (cashIn) ? cashIn : 0,
      lines: [{ type: 'beer', quantity: beers }]
    }
  }

  executeIfEnoughFunds(tx: TransactionReq): Observable<TransactionResult> {
    return this.api.post<TransactionResult>('wallet/transaction', tx);
  }

  fetchStatistics() {
    return this.api.get<TransactionStatistics>('statistics');
  }
}
