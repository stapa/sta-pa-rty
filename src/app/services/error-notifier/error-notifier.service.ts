import { HttpErrorResponse } from '@angular/common/http';
import { Injectable, inject } from '@angular/core';
import { ToastController } from '@ionic/angular/standalone';

@Injectable({
  providedIn: 'root'
})
export class ErrorNotifierService {
  private toastCtrl = inject(ToastController);

  async notifyError(error: any) {
    let errorMsg;
    if(error instanceof HttpErrorResponse) {
      if(error.status === 0) {
        errorMsg = `Connection lost! Please check your internet connection`;
      } else {
        errorMsg = `Something bad happened; please try again later. (${error.status}: ${error.message})`;
      }
    } else {
      errorMsg = `${error}`;
    }
    const toast = await this.toastCtrl.create({
      message: errorMsg,
      duration: 5000,
      position: 'top',
      color: 'danger'
    });
    await toast.present();
  }

}
