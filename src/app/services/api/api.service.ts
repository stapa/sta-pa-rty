import { Injectable, inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { SecureStoragePlugin } from 'capacitor-secure-storage-plugin';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  useQaBackend = false;
  private http = inject(HttpClient);

  constructor() {
    this.initDevFlags();
  }

  private initDevFlags() {
    SecureStoragePlugin.get({key: 'devFlags_useQaBackend'})
      .then((res) => {
        if (res.value === "ON") {
          this.useQaBackend = true;
        }
      })
      .catch(() => {
        this.useQaBackend = false;
        SecureStoragePlugin.set({key: 'devFlags_useQaBackend', value: 'OFF'})
      });
      console.log(
        `[Service: Api] (init) devFlags_useQaBackend: ${this.useQaBackend}`
      );
  }

  setDevFlags(flags: {useQaBackend?: boolean}) {
    if (flags.useQaBackend !== undefined) {
      this.useQaBackend = flags.useQaBackend;
      SecureStoragePlugin.set({
        key: 'devFlags_useQaBackend',
        value: flags.useQaBackend ? 'ON' : 'OFF'
      });
      console.log(
        `[Service: Api] set devFlags_useQaBackend: ${this.useQaBackend}`
      );
    }
  }

  private url(target: string) {
    return (
      (this.useQaBackend ? environment.baseUrlQA : environment.baseUrl)
      + '/'
      + target
    )
  }

  get<T>(target: string, params?: any) {
    return this.http.get<T>(this.url(target), {
      params: params,
      withCredentials: true
    });
  }

  post<T>(target: string, body: any|null, params?: any) {
    return this.http.post<T>(this.url(target), body, {
      params: params,
      withCredentials: true
    });
  }

}
