export const environment = {
  appVersion: require('../../package.json').version,
  deployment: 'production',
  production: true,
  // baseUrl: backend
  baseUrl: "https://api.stapa.fr",
  baseUrlQA: "https://api-qa.stapa.fr",
  // appUrl: url for webapp and deeplinking
  appUrl: "https://app.stapa.fr",
  // Stripe locationId for NFC Terminal
  stripeLocationId: "tml_FNxG5QHxFx2feo"
};
