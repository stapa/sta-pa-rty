export const environment = {
  appVersion: require('../../package.json').version + '--staging',
  deployment: 'staging',
  production: false,
  // baseUrl: backend
  baseUrl: "https://api-test.stapa.fr",
  baseUrlQA: "https://api-qa.stapa.fr",
  // appUrl: url for webapp and deeplinking
  appUrl: "https://app-test.stapa.fr",
  // Stripe locationId for NFC Terminal
  stripeLocationId: "tml_FBwDQQaEuDa3gl"
};
